const request = require('request');
const ping = require('jjg-ping');

main();

async function main(){
	var myip = await ifconfig();

	while(1){
		var this_target = await list_target();
		// console.log(this_target.body);
		var json = JSON.parse(this_target.body);

		run_process(json, myip);
		await sleep(60000);
	}
}

async function run_process(json, myip){
	for(x in json){
		var this_ip = json[x];
		var ping_result = await process_ping(this_ip);

		if(ping_result != "x"){
			var url_save = "http://203.210.84.102:5052/ping-server/public/ping?source="+myip+"&target="+this_ip+"&ping="+ping_result;
			console.log(new Date, "done test", this_ip, "ping "+ping_result+"ms")
		}else{
			var url_save = "http://203.210.84.102:5052/ping-server/public/ping?source="+myip+"&target="+this_ip+"&ping="+ping_result;
			console.log(new Date, "FAIL! test", this_ip)
		}
		await save_data(url_save);
	}
}

function process_ping(ip_target){
    return new Promise(function(resolve, reject){
		ping.system.ping(ip_target, async function(latency, status) {
			if(status){
				return resolve(latency);
			}else{
				return resolve("x");
			}
		})
    });
};


function list_target(){
    return new Promise(function(resolve, reject){
		request('http://203.210.84.102:5052/ping-server/public/target', function (error, response, body) {
			return resolve(response);
		});
    });
};

function save_data(url){
    return new Promise(function(resolve, reject){
		request(url, function (error, response, body) {
			return resolve(response);
		});
    });
};

function ifconfig(){
    return new Promise(function(resolve, reject){
		request('http://ifconfig.me', function (error, response, body) {
			return resolve(body);
		});
    });
};

function sleep(time){
	return new Promise(resolve => setTimeout(resolve, time));
}